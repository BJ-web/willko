import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:willko/Success_check.dart';
import 'package:willko/Success_depart.dart';

const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';

class Staff_depart extends StatefulWidget {
  @override
  _Staff_depart createState() => _Staff_depart();
}

class _Staff_depart extends State<Staff_depart> {

  final myController = TextEditingController();
  Future<String> _barcodeString;
  QRViewController controller;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var flashState = flashOff;
  var cameraState = frontCamera;
  bool _isButtonDisabled;

  @override
  void initState() {
    _isButtonDisabled = true;
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        myController.text = scanData;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(40),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                      ),
                      Text("     Exodellices HQ",style: TextStyle(fontSize: 50,color: Color(0xffC2342E),fontFamily: "Poppins-medium")),
                      ButtonTheme(
                        padding: EdgeInsets.all(5.0),
                        minWidth: 120,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              side: BorderSide(color: _isButtonDisabled?Color(0xffBEBEBE):Color(0xffC2342E))),
                          onPressed: () {
                            Navigator.push(
                                context, MaterialPageRoute(builder: (context) => Success_depart()));
                          },
                          color: _isButtonDisabled?Color(0xffBEBEBE):Color(0xffC2342E),
                          textColor: Colors.white,
                          child: Text("NEXT".toUpperCase(),
                              style: TextStyle(fontSize: 25, fontFamily: "Poppins")),
                        ),
                      )
                    ],
                  ),
                  Center(
                    child: Text("Use your qrCode",style: TextStyle(fontSize: 40,color: Color(0xffC2342E),fontFamily: "Sofia Pro")),
                  ),
                  SizedBox(height: 50),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 320,
                        height: 320,
                        decoration: new BoxDecoration(
                            color: Color(0xffFFFFFF).withOpacity(0.3),
                            border: Border.all(color: Color(0xffBEBEBE),width: 1.0)
                        ),
                        child: QRView(
                          key: qrKey,
                          onQRViewCreated: _onQRViewCreated,
                          overlay: QrScannerOverlayShape(
                            borderRadius: 0,
                            borderLength: 0,
                            borderWidth: 0,
                            cutOutSize: 320,
                          ),
                        ),
                      ),
                      SizedBox(width: 50),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: 2,
                            height: 145,
                            color: Colors.white.withOpacity(0.5),
                          ),
                          Text("OR",style: TextStyle(fontSize: 23.0,
                              color: Colors.white,
                              fontFamily: "Poppins-medium")),
                          Container(
                            width: 2,
                            height: 145,
                            color: Colors.white.withOpacity(0.5),
                          )
                        ],
                      ),
                      SizedBox(width: 50),
                      Column(
                        children: <Widget>[
                          Container(
                            width: 400,
                            height: 80,
                            decoration: new BoxDecoration(
                                color: Color(0xffF9C933),
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 2,
                                    blurRadius: 2,
                                    offset: Offset(1, 3), // changes position of shadow
                                  ),
                                ]
                            ),
                            child: Container(
                              width: 400,
                              height: 80,
                              decoration: new BoxDecoration(
                                color: Color(0xffFFFFFF).withOpacity(0.4),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Padding(
                                padding: EdgeInsets.only(left:25.0,top: 7.0,right: 7.0,bottom: 7.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: TextField(
                                    onChanged: (text){
                                      if(text.length!=0){
                                        myController.text = text;
                                        setState(() {
                                          _isButtonDisabled = false;
                                        });
                                      }else{
                                        setState(() {
                                          _isButtonDisabled = true;
                                        });
                                      }
                                    },
                                    controller: myController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Email, mobile or digicode',
                                        hintStyle: TextStyle(fontSize: 23.0,
                                            color: Color(0xffC2342E),
                                            fontFamily: "Poppins-medium")
                                    ),
                                    style: TextStyle(fontSize: 23.0,
                                        color: Color(0xffC2342E),
                                        fontFamily: "Poppins-medium"),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Padding(
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("Enter your identity digicode or",style: TextStyle(fontSize: 23.0,
                                      color: Color(0xffC2342E),
                                      fontFamily: "Poppins-medium")),
                                  Text("your mail or mobile phone",style: TextStyle(fontSize: 23.0,
                                      color: Color(0xffC2342E),
                                      fontFamily: "Poppins-medium"))
                                ],
                              ),
                            ),
                            padding: EdgeInsets.only(right: 47),
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 190),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset("images/GroupeLogo.png", width: 90),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

}

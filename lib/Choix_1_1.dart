import 'package:flutter/material.dart';
import 'package:willko/Coordonnees.dart';
import 'package:willko/Delivery_for.dart';
import 'package:willko/Success_check.dart';
import 'package:willko/Take_pic.dart';

class Choix1_1 extends StatefulWidget {
  @override
  _Choix1_1 createState() => _Choix1_1();
}

class _Choix1_1 extends State<Choix1_1> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Padding(
        padding: EdgeInsets.all(40),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    GestureDetector(
                      child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),
                    SizedBox(width: 250,),
                    Text("   DHL GABON",style: TextStyle(fontSize: 50,color: Color(0xffC2342E),fontFamily: "Poppins-medium")),
                  ],
                ),
                Center(
                  child: Text("Sélectionnez la raison de votre visite",style: TextStyle(fontSize: 40,color: Color(0xffC2342E),fontFamily: "Sofia Pro")),
                ),
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      child: Container(
                        width: 320,
                        height: 200,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xffF9C933),
                            border: Border.all(color:Color(0xffF9C933)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 2,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Container(
                          width: 320,
                          height: 200,
                          decoration: new BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffFFFFFF).withOpacity(0.3),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(7.0),
                            child: Center(
                              child: Text(
                                "Réunion",style: TextStyle(fontSize: 30.0,
                                  color: Color(0xffC2342E),
                                  fontFamily: "Poppins"),textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Coordonnees()));
                      },
                    ),
                    SizedBox(width: 50),
                    GestureDetector(
                      child: Container(
                        width: 320,
                        height: 200,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xffF9C933),
                            border: Border.all(color:Color(0xffF9C933)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 2,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Container(
                          width: 320,
                          height: 200,
                        decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xffFFFFFF).withOpacity(0.3),
                        ),
                          child: Padding(
                            padding: EdgeInsets.all(7.0),
                            child: Center(
                              child: Text(
                                "Interview",style: TextStyle(fontSize: 30.0,
                                  color: Color(0xffC2342E),
                                  fontFamily: "Poppins"),textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Coordonnees()));
                      },
                    )
                  ],
                ),
                SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      child: Container(
                        width: 320,
                        height: 200,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xffF9C933),
                            border: Border.all(color:Color(0xffF9C933)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 2,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Container(
                          width: 320,
                          height: 200,
                          decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xffFFFFFF).withOpacity(0.3),
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(7.0),
                            child: Center(
                              child: Text(
                                "Entretien d’embauche",style: TextStyle(fontSize: 30.0,
                                  color: Color(0xffC2342E),
                                  fontFamily: "Poppins"),textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Coordonnees()));
                      },
                    ),
                    SizedBox(width: 50),
                    GestureDetector(
                      child: Container(
                        width: 320,
                        height: 200,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xffF9C933),
                            border: Border.all(color:Color(0xffF9C933)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 2,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Container(
                        width: 320,
                        height: 200,
                        decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xffFFFFFF).withOpacity(0.3),
                        ),
                          child: Padding(
                            padding: EdgeInsets.all(7.0),
                            child: Center(
                              child: Text(
                                "Personnelle",style: TextStyle(fontSize: 30.0,
                                  color: Color(0xffC2342E),
                                  fontFamily: "Poppins"),textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Coordonnees()));
                      },
                    )
                  ],
                )
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset("images/GroupeLogo.png", width: 90),
            )
          ],
        ),
      ),
    );
  }

}

import 'package:flutter/material.dart';
import 'package:willko/Delivery_for.dart';
import 'package:willko/Rdv_with.dart';
import 'package:willko/Success_check.dart';
import 'package:willko/Take_pic.dart';

class Coordonnees extends StatefulWidget {
  final String imagePath;
  final String nom,prenom,entreprise,phone,mail;
  const Coordonnees({Key key, this.imagePath,this.entreprise,this.nom,this.mail,this.phone,this.prenom}) : super(key: key);

  @override
  _Coordonnees createState() => _Coordonnees(imagePath: imagePath,entreprise_: entreprise,nom_: nom,mail_: mail,phone_: phone,prenom_: prenom);
}

class _Coordonnees extends State<Coordonnees> {
  final String imagePath;
  final String nom_,entreprise_,phone_,mail_,prenom_;
  _Coordonnees({Key key, this.imagePath,this.entreprise_,this.nom_,this.mail_,this.phone_,this.prenom_});

  final entreprise = TextEditingController();
  final nom = TextEditingController();
  final prenom = TextEditingController();
  final numero = TextEditingController();
  final mail = TextEditingController();

  bool _isButtonDisabled;
  bool isSwitched = false;

  @override
  void initState() {
    _isButtonDisabled = true;
    if(nom_!=null){
      nom.text = nom_;
    }
    if(entreprise_!=null){
      entreprise.text = entreprise_;
    }
    if(mail_!=null){
      mail.text = mail_;
    }
    if(phone_!=null){
      numero.text = phone_;
    }

    if(mail.text.length!=0 && nom.text.length!=0 && numero.text.length!=0 && entreprise.text.length!=0 && imagePath!=null){
      setState(() {
        _isButtonDisabled = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(40),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                      ),
                      Text("     DHL GABON",style: TextStyle(fontSize: 50,color: Color(0xffC2342E),fontFamily: "Poppins-medium")),
                      ButtonTheme(
                        padding: EdgeInsets.all(5.0),
                        minWidth: 140,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              side: BorderSide(color: _isButtonDisabled?Color(0xffBEBEBE):Color(0xffC2342E))),
                          onPressed: () {
                            Navigator.push(_isButtonDisabled?null:
                            context, MaterialPageRoute(builder: (context) => Rdv_with()));
                          },
                          color: _isButtonDisabled?Color(0xffBEBEBE):Color(0xffC2342E),
                          textColor: Colors.white,
                          child: Text("SUIVANT".toUpperCase(),
                              style: TextStyle(fontSize: 25, fontFamily: "Poppins")),
                        ),
                      )
                    ],
                  ),
                  Center(
                    child: Text("Entrez vos coordonnées",style: TextStyle(fontSize: 40,color: Color(0xffC2342E),fontFamily: "Sofia Pro")),
                  ),
                  SizedBox(height: 50),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Align(
                              child: GestureDetector(
                                child: Container(
                                  width: 200,
                                  height: 200,
                                  decoration: new BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(color: Color(0xffC2342E),width: 2.0)
                                  ),
                                  child: imagePath==null?Center(
                                    child: Padding(
                                      padding: EdgeInsets.all(5.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Icon(Icons.perm_identity,color: Color(0xffC2342E),size: 90),
                                          Text(
                                            "Prendre la photo",style: TextStyle(fontSize: 18.0,
                                              color: Color(0xffC2342E),
                                              fontFamily: "Poppins-medium"),textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 20,)
                                        ],
                                      ),
                                    ),
                                  ):CircleAvatar(
                                    backgroundImage: AssetImage(imagePath),radius: 100,
                                  ),
                                ),
                                onTap: (){
                                  Navigator.push(
                                      context, MaterialPageRoute(builder: (context) => TakePictureScreen(nom: nom.text,entreprise: entreprise.text,phone: numero.text,mail: mail.text)));
                                },
                              ),
                              alignment: Alignment.topLeft,
                            ),
                            SizedBox(height: 40),

                          ],
                        ),
                      ),
                      SizedBox(width: 30),
                      Flexible(
                        flex: 3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: 320,
                                  height: 80,
                                  decoration: new BoxDecoration(
                                      color: Color(0xffF9C933),
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black12,
                                          spreadRadius: 2,
                                          blurRadius: 2,
                                          offset: Offset(1, 3), // changes position of shadow
                                        ),
                                      ]
                                  ),
                                  child: Container(
                                    width: 320,
                                    height: 80,
                                    decoration: new BoxDecoration(
                                      color: Color(0xffFFFFFF).withOpacity(0.4),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(7.0),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: TextField(
                                          keyboardType: TextInputType.text,
                                          onChanged: (text){
                                            if(text.length!=0 && prenom.text.length!=0 && numero.text.length!=0 && mail.text.length!=0 && imagePath!=null){
                                              nom.text = text;
                                              setState(() {
                                                _isButtonDisabled = false;
                                              });
                                            }else{
                                              setState(() {
                                                _isButtonDisabled = true;
                                              });
                                            }
                                          },
                                          controller: nom,
                                          decoration: InputDecoration(
                                              prefixIcon: Padding(
                                                child: Icon(Icons.perm_identity,color: Color(0xffC2342E),size: 35),
                                                padding: EdgeInsets.only(right: 10),
                                              ),
                                              border: InputBorder.none,
                                              hintText: 'Nom',
                                              hintStyle: TextStyle(fontSize: 23.0,
                                                  color: Color(0xffC2342E),
                                                  fontFamily: "Poppins-medium")
                                          ),
                                          style: TextStyle(fontSize: 23.0,
                                              color: Color(0xffC2342E),
                                              fontFamily: "Poppins-medium"),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  width: 320,
                                  height: 80,
                                  decoration: new BoxDecoration(
                                      color: Color(0xffF9C933),
                                      borderRadius: BorderRadius.circular(5),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.black12,
                                          spreadRadius: 2,
                                          blurRadius: 2,
                                          offset: Offset(1, 3), // changes position of shadow
                                        ),
                                      ]
                                  ),
                                  child: Container(
                                    width: 320,
                                    height: 80,
                                    decoration: new BoxDecoration(
                                      color: Color(0xffFFFFFF).withOpacity(0.4),
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.all(7.0),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: TextField(
                                          keyboardType: TextInputType.text,
                                          onChanged: (text){
                                            if(text.length!=0 && nom.text.length!=0 && numero.text.length!=0 && mail.text.length!=0 && imagePath!=null){
                                              nom.text = text;
                                              setState(() {
                                                _isButtonDisabled = false;
                                              });
                                            }else{
                                              setState(() {
                                                _isButtonDisabled = true;
                                              });
                                            }
                                          },
                                          controller: prenom,
                                          decoration: InputDecoration(
                                              prefixIcon: Padding(
                                                child: Icon(Icons.perm_identity,color: Color(0xffC2342E),size: 35),
                                                padding: EdgeInsets.only(right: 10),
                                              ),
                                              border: InputBorder.none,
                                              hintText: 'Prénom',
                                              hintStyle: TextStyle(fontSize: 23.0,
                                                  color: Color(0xffC2342E),
                                                  fontFamily: "Poppins-medium")
                                          ),
                                          style: TextStyle(fontSize: 23.0,
                                              color: Color(0xffC2342E),
                                              fontFamily: "Poppins-medium"),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(height: 40),
                            Container(
                              width: 700,
                              height: 80,
                              decoration: new BoxDecoration(
                                  color: Color(0xffF9C933),
                                  borderRadius: BorderRadius.circular(5),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black12,
                                      spreadRadius: 2,
                                      blurRadius: 2,
                                      offset: Offset(1, 3), // changes position of shadow
                                    ),
                                  ]
                              ),
                              child: Container(
                                width: 700,
                                height: 80,
                                decoration: new BoxDecoration(
                                  color: Color(0xffFFFFFF).withOpacity(0.4),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.all(7.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: TextField(
                                      keyboardType: TextInputType.emailAddress,
                                      onChanged: (text){
                                        if(text.length!=0 && nom.text.length!=0 && numero.text.length!=0 && prenom.text.length!=0 && imagePath!=null){
                                          mail.text = text;
                                          setState(() {
                                            _isButtonDisabled = false;
                                          });
                                        }else{
                                          setState(() {
                                            _isButtonDisabled = true;
                                          });
                                        }
                                      },
                                      controller: mail,
                                      decoration: InputDecoration(
                                          prefixIcon: Padding(
                                            child: Image.asset("images/adress.png",color: Color(0xffC2342E)),
                                            padding: EdgeInsets.only(right: 10),
                                          ),
                                          border: InputBorder.none,
                                          hintText: 'Adresse mail',
                                          hintStyle: TextStyle(fontSize: 23.0,
                                              color: Color(0xffC2342E),
                                              fontFamily: "Poppins-medium")
                                      ),
                                      style: TextStyle(fontSize: 23.0,
                                          color: Color(0xffC2342E),
                                          fontFamily: "Poppins-medium"),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 40),
                          ],
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Container(
                            width: 450,
                            height: 80,
                            decoration: new BoxDecoration(
                                color: Color(0xffF9C933),
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 2,
                                    blurRadius: 2,
                                    offset: Offset(1, 3), // changes position of shadow
                                  ),
                                ]
                            ),
                            child: Container(
                              width: 450,
                              height: 80,
                              decoration: new BoxDecoration(
                                color: Color(0xffFFFFFF).withOpacity(0.4),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(7.0),
                                child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: TextField(
                                    keyboardType: TextInputType.text,
                                    onChanged: (text){
                                      if(prenom.text.length!=0 && nom.text.length!=0 && numero.text.length!=0 && mail.text.length!=0 && imagePath!=null){
                                        entreprise.text = text;
                                        setState(() {
                                          _isButtonDisabled = false;
                                        });
                                      }else{
                                        setState(() {
                                          _isButtonDisabled = true;
                                        });
                                      }
                                    },
                                    controller: entreprise,
                                    decoration: InputDecoration(
                                        prefixIcon: Padding(
                                          child: Image.asset("images/building_icon.png",color: Color(0xffC2342E)),
                                          padding: EdgeInsets.only(right: 10),
                                        ),
                                        border: InputBorder.none,
                                        hintText: 'Votre Entreprise (Optionnel)',
                                        hintStyle: TextStyle(fontSize: 23.0,
                                            color: Color(0xffC2342E),
                                            fontFamily: "Poppins-medium")
                                    ),
                                    style: TextStyle(fontSize: 23.0,
                                        color: Color(0xffC2342E),
                                        fontFamily: "Poppins-medium"),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Container(
                            width: 450,
                            height: 80,
                            decoration: new BoxDecoration(
                                color: Color(0xffF9C933),
                                borderRadius: BorderRadius.circular(5),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.black12,
                                    spreadRadius: 2,
                                    blurRadius: 2,
                                    offset: Offset(1, 3), // changes position of shadow
                                  ),
                                ]
                            ),
                            child: Container(
                              width: 450,
                              height: 80,
                              decoration: new BoxDecoration(
                                color: Color(0xffFFFFFF).withOpacity(0.4),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Padding(
                                padding: EdgeInsets.all(7.0),
                                child: Row(
                                  children: <Widget>[
                                    Flexible(
                                      child: Image.asset("images/phone.png",width: 35,color: Color(0xffC2342E)),
                                      flex: 1,
                                    ),
                                    Flexible(
                                      child: TextField(
                                        keyboardType: TextInputType.phone,
                                        onChanged: (text){
                                          if(text.length!=0 && nom.text.length!=0 && prenom.text.length!=0 && mail.text.length!=0 && imagePath!=null){
                                            numero.text = text;
                                            setState(() {
                                              _isButtonDisabled = false;
                                            });
                                          }else{
                                            setState(() {
                                              _isButtonDisabled = true;
                                            });
                                          }
                                        },
                                        controller: numero,
                                        decoration: InputDecoration(
                                            prefixIcon:Image.asset("images/ga.png"),
                                            border: InputBorder.none,
                                            hintText: 'Numéro de téléphone',
                                            hintStyle: TextStyle(fontSize: 23.0,
                                                color: Color(0xffC2342E),
                                                fontFamily: "Poppins-medium")
                                        ),
                                        style: TextStyle(fontSize: 23.0,
                                            color: Color(0xffC2342E),
                                            fontFamily: "Poppins-medium"),
                                      ),
                                      flex: 3,
                                    )

                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(right: 66),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Switch(
                          value: isSwitched,
                          onChanged: (value){
                            setState(() {
                              isSwitched=value;
                            });
                          },
                          activeTrackColor: Color(0xffFFFFFF).withOpacity(0.3),
                          inactiveTrackColor: Color(0xffFFFFFF).withOpacity(0.3),
                          activeColor: Color(0xffC2342E),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(child:
                        Text("Enregistrer mes informations pour accélérer mon prochain enregistrement",style: TextStyle(fontFamily: "Poppins-medium", color: Color(0xffC2342E),fontSize: 23))),
                      ],
                    ),
                  ),
                  SizedBox(height: 100),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset("images/GroupeLogo.png", width: 90),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

}

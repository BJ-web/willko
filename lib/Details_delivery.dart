import 'package:flutter/material.dart';
import 'package:willko/Delivery_for.dart';
import 'package:willko/Success_check.dart';

class Details_delivery extends StatefulWidget {
  @override
  _Details_delivery createState() => _Details_delivery();
}

class _Details_delivery extends State<Details_delivery> {

  final societe = TextEditingController();
  final colis = TextEditingController();

  bool _isButtonDisabled;
  String statut_btn = "aucun";
  bool isSwitched = false;

  @override
  void initState() {
    _isButtonDisabled = true;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(40),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                      ),
                      Text("     Exodellices HQ",style: TextStyle(fontSize: 50,color: Color(0xffC2342E),fontFamily: "Poppins-medium")),
                      ButtonTheme(
                        padding: EdgeInsets.all(5.0),
                        minWidth: 120,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              side: BorderSide(color: _isButtonDisabled?Color(0xffBEBEBE):Color(0xffC2342E))),
                          onPressed: () {
                            Navigator.push(_isButtonDisabled?null:
                            context, MaterialPageRoute(builder: (context) => Delivery_for()));
                          },
                          color: _isButtonDisabled?Color(0xffBEBEBE):Color(0xffC2342E),
                          textColor: Colors.white,
                          child: Text("NEXT".toUpperCase(),
                              style: TextStyle(fontSize: 25, fontFamily: "Poppins")),
                        ),
                      )
                    ],
                  ),
                  Center(
                    child: Text("Enter details of delivery",style: TextStyle(fontSize: 40,color: Color(0xffC2342E),fontFamily: "Sofia Pro")),
                  ),
                  SizedBox(height: 80),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 400,
                        height: 80,
                        decoration: new BoxDecoration(
                            color: Color(0xffF9C933),
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 2,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Container(
                            decoration: new BoxDecoration(
                              color: Color(0xffFFFFFF).withOpacity(0.4),
                              borderRadius: BorderRadius.circular(5),
                            ),
                          child: Padding(
                            padding: EdgeInsets.all(7.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: TextField(
                                onChanged: (text){
                                  if(text.length!=0 && colis.text.length!=0 && statut_btn != "aucun"){
                                    societe.text = text;
                                    setState(() {
                                      _isButtonDisabled = false;
                                    });
                                  }else{
                                    setState(() {
                                      _isButtonDisabled = true;
                                    });
                                  }
                                },
                                controller: societe,
                                decoration: InputDecoration(
                                    prefixIcon: Padding(
                                      child: Image.asset("images/delivery.png"),
                                      padding: EdgeInsets.only(right: 20),
                                    ),
                                    border: InputBorder.none,
                                    hintText: 'Société de livraison',
                                    hintStyle: TextStyle(fontSize: 23.0,
                                        color: Color(0xffC2342E),
                                        fontFamily: "Poppins-medium")
                                ),
                                style: TextStyle(fontSize: 23.0,
                                    color: Color(0xffC2342E),
                                    fontFamily: "Poppins-medium"),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                          width: 30
                      ),
                      Container(
                        width: 400,
                        height: 80,
                        decoration: new BoxDecoration(
                            color: Color(0xffF9C933),
                            borderRadius: BorderRadius.circular(5),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.black12,
                                spreadRadius: 2,
                                blurRadius: 2,
                                offset: Offset(1, 3), // changes position of shadow
                              ),
                            ]
                        ),
                        child: Container(
                            width: 400,
                            height: 80,
                            decoration: new BoxDecoration(
                              color: Color(0xffFFFFFF).withOpacity(0.4),
                              borderRadius: BorderRadius.circular(5),
                            ),
                          child: Padding(
                            padding: EdgeInsets.all(7.0),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: TextField(
                                keyboardType: TextInputType.number,
                                onChanged: (text){
                                  if(text.length!=0 && societe.text.length!=0 && statut_btn != "aucun"){
                                    colis.text = text;
                                    setState(() {
                                      _isButtonDisabled = false;
                                    });
                                  }else{
                                    setState(() {
                                      _isButtonDisabled = true;
                                    });
                                  }
                                },
                                controller: colis,
                                decoration: InputDecoration(
                                    prefixIcon: Padding(
                                      child: Image.asset("images/cargo-box.png"),
                                      padding: EdgeInsets.only(right: 20),
                                    ),
                                    border: InputBorder.none,
                                    hintText: 'Numéro de colis',
                                    hintStyle: TextStyle(fontSize: 23.0,
                                        color: Color(0xffC2342E),
                                        fontFamily: "Poppins-medium")
                                ),
                                style: TextStyle(fontSize: 23.0,
                                    color: Color(0xffC2342E),
                                    fontFamily: "Poppins-medium"),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          width: 220,
                          height: 80,
                          decoration: new BoxDecoration(
                              color: Color(0xffF9C933),
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  offset: Offset(1, 3), // changes position of shadow
                                ),
                              ]
                          ),
                          child: Container(
                              width: 220,
                              height: 80,
                              decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Color(0xffFFFFFF).withOpacity(0.4),
                                  border: Border.all(color: statut_btn=="courrier"?Color(0xffC2342E):Color(0xffFFFFFF).withOpacity(0.3),width:statut_btn=="courrier"?3:0)
                              ),
                            child: Padding(
                              padding: EdgeInsets.all(7.0),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "Courrier",style: TextStyle(fontSize: 23.0,
                                    color: Color(0xffC2342E),
                                    fontFamily: "Poppins-medium"),
                                ),
                              ),
                            ),
                          ),
                        ),
                        onTap: (){
                          setState(() {
                            statut_btn = "courrier";
                            if(colis.text.length!=0 && societe.text.length!=0 && statut_btn != "aucun"){
                              setState(() {
                                _isButtonDisabled = false;
                              });
                            }else{
                              setState(() {
                                _isButtonDisabled = true;
                              });
                            }
                          });
                        },
                      ),
                      SizedBox(
                          width: 85
                      ),
                      GestureDetector(
                        child: Container(
                          width: 220,
                          height: 80,
                          decoration: new BoxDecoration(
                              color: Color(0xffF9C933),
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  offset: Offset(1, 3), // changes position of shadow
                                ),
                              ]
                          ),
                          child: Container(
                              width: 220,
                              height: 80,
                              decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Color(0xffFFFFFF).withOpacity(0.4),
                                  border: Border.all(color: statut_btn=="colis"?Color(0xffC2342E):Color(0xffFFFFFF).withOpacity(0.3),width:statut_btn=="colis"?3:0)
                              ),
                            child: Padding(
                              padding: EdgeInsets.all(7.0),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "Colis",style: TextStyle(fontSize: 23.0,
                                    color: Color(0xffC2342E),
                                    fontFamily: "Poppins-medium"),
                                ),
                              ),
                            ),
                          ),
                        ),
                        onTap: (){
                          setState(() {
                            statut_btn = "colis";
                            if(colis.text.length!=0 && societe.text.length!=0 && statut_btn != "aucun"){
                              setState(() {
                                _isButtonDisabled = false;
                              });
                            }else{
                              setState(() {
                                _isButtonDisabled = true;
                              });
                            }
                          });
                        },
                      ),
                      SizedBox(
                          width: 85
                      ),
                      GestureDetector(
                        child: Container(
                          width: 220,
                          height: 80,
                          decoration: new BoxDecoration(
                              color: Color(0xffF9C933),
                              borderRadius: BorderRadius.circular(5),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.black12,
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  offset: Offset(1, 3), // changes position of shadow
                                ),
                              ]
                          ),
                          child: Container(
                              width: 220,
                              height: 80,
                              decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: Color(0xffFFFFFF).withOpacity(0.4),
                                  border: Border.all(color: statut_btn=="autres"?Color(0xffC2342E):Color(0xffFFFFFF).withOpacity(0.3),width:statut_btn=="autres"?3:0)
                              ),
                            child: Padding(
                              padding: EdgeInsets.all(7.0),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  "Autres",style: TextStyle(fontSize: 23.0,
                                    color: Color(0xffC2342E),
                                    fontFamily: "Poppins-medium"),
                                ),
                              ),
                            ),
                          ),
                        ),
                        onTap: (){
                          setState(() {
                            statut_btn = "autres";
                            if(colis.text.length!=0 && societe.text.length!=0 && statut_btn != "aucun"){
                              setState(() {
                                _isButtonDisabled = false;
                              });
                            }else{
                              setState(() {
                                _isButtonDisabled = true;
                              });
                            }
                          });
                        },
                      )
                    ],
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(left: 45),
                    child: Row(
                      children: <Widget>[
                        Switch(
                          value: isSwitched,
                          onChanged: (value){
                            setState(() {
                              isSwitched=value;
                            });
                          },
                          activeTrackColor: Color(0xffFFFFFF).withOpacity(0.3),
                          inactiveTrackColor: Color(0xffFFFFFF).withOpacity(0.3),
                          activeColor: Color(0xffC2342E),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text("Require a signature of recipient",style: TextStyle(fontFamily: "Poppins-medium", color: Color(0xffC2342E),fontSize: 23))
                      ],
                    ),
                  ),
                  SizedBox(height: 230),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset("images/GroupeLogo.png", width: 90),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

}

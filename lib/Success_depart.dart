import 'package:flutter/material.dart';
import 'package:willko/TouchScreen.dart';

class Success_depart extends StatefulWidget {
  @override
  _Success_depart createState() => _Success_depart();
}

class _Success_depart extends State<Success_depart> {

  @override
  void initState() {
    super.initState();
    new Future.delayed(const Duration(seconds: 6), () => Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => (Touch())),
            (Route<dynamic> route) => false));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: new BoxDecoration(
          color: Color(0xffFFFFFF).withOpacity(0.7),
        ),
        child: Padding(
          padding: EdgeInsets.all(7.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage(
                        "images/homme-noir.jpg",
                      ),
                      radius: 100,
                    ),

                    Positioned(
                      bottom: 0.0,
                      right: 30.0,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(19),
                        ),
                        height: 30,
                        width: 30,
                        child: Center(
                          child: Container(
                            decoration: BoxDecoration(
                              color: Color(0xffE62121),
                              borderRadius: BorderRadius.circular(19),
                            ),
                            height: 30,
                            width: 30,
                          )
                          ,
                        ),
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 10),
                Text("THANK YOU Claude", style: TextStyle(fontSize: 70,fontFamily: "Poppins Bold",color: Colors.black)),
                SizedBox(height: 20),
                Text("You’re now checked-out", style: TextStyle(fontSize: 40,fontFamily: "Poppins-medium",color: Colors.black))
              ],
            ),
          ),
        ),
      ),
    );
  }

}

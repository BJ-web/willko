import 'package:flutter/material.dart';
import 'package:willko/Choix_1_1.dart';
import 'package:willko/Delivery_for.dart';
import 'package:willko/Invitation.dart';
import 'package:willko/Success_check.dart';

class Choix1 extends StatefulWidget {
  @override
  _Choix1 createState() => _Choix1();
}

class _Choix1 extends State<Choix1> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Padding(
        padding: EdgeInsets.all(60),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                ),
                SizedBox(height: 80),
                GestureDetector(
                  child: Container(
                    width: 600,
                    height: 120,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xffF9C933),
                        border: Border.all(color:Color(0xffF9C933)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 2,
                            blurRadius: 2,
                            offset: Offset(1, 3), // changes position of shadow
                          ),
                        ]
                    ),
                    child: Container(
                        width: 600,
                        height: 120,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: Color(0xffFFFFFF).withOpacity(0.3),
                        ),
                      child: Padding(
                        padding: EdgeInsets.all(7.0),
                        child: Row(
                          children: <Widget>[
                            SizedBox(width: 60,),
                            Icon(Icons.perm_identity,color: Color(0xffC2342E),size: 40),
                            SizedBox(width: 40,),
                            Text(
                              "C’est la première fois",style: TextStyle(fontSize: 30.0,
                                color: Color(0xffC2342E),
                                fontFamily: "Poppins-medium"),textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                    Navigator.push(
                    context, MaterialPageRoute(builder: (context) => Choix1_1()));
                  },
                ),
                SizedBox(height: 30),
                GestureDetector(
                  child: Container(
                    width: 600,
                    height: 120,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xffF9C933),
                        border: Border.all(color:Color(0xffF9C933)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 2,
                            blurRadius: 2,
                            offset: Offset(1, 3), // changes position of shadow
                          ),
                        ]
                    ),
                    child: Container(
                        width: 600,
                        height: 120,
                        decoration: new BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffFFFFFF).withOpacity(0.3),
                        ),
                      child: Padding(
                        padding: EdgeInsets.all(7.0),
                        child:Row(
                          children: <Widget>[
                            SizedBox(width: 60,),
                            Image.asset("images/repeat.png",width: 40),
                            SizedBox(width: 40,),
                            Text(
                              "Vous êtes déjà venu",style: TextStyle(fontSize: 30.0,
                                color: Color(0xffC2342E),
                                fontFamily: "Poppins-medium"),textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                  },
                ),
                SizedBox(height: 30),
                GestureDetector(
                  child: Container(
                    width: 600,
                    height: 120,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xffF9C933),
                        border: Border.all(color:Color(0xffF9C933)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 2,
                            blurRadius: 2,
                            offset: Offset(1, 3), // changes position of shadow
                          ),
                        ]
                    ),
                    child: Container(
                        width: 600,
                        height: 120,
                        decoration: new BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffFFFFFF).withOpacity(0.3),
                        ),
                      child: Padding(
                          padding: EdgeInsets.all(7.0),
                          child: Row(
                            children: <Widget>[
                              SizedBox(width: 60,),
                              Image.asset("images/right-chevron.png",width: 40),
                              SizedBox(width: 40,),
                              Text(
                                "Vous avez une invitation",style: TextStyle(fontSize: 30.0,
                                  color: Color(0xffC2342E),
                                  fontFamily: "Poppins-medium"),textAlign: TextAlign.center,
                              )
                            ],
                          )
                      ),
                    ),
                  ),
                  onTap: (){
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => Invitation()));
                  },
                )
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset("images/GroupeLogo.png", width: 90),
            )
          ],
        ),
      ),
    );
  }

}

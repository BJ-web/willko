import 'package:flutter/material.dart';
import 'package:willko/TouchScreen.dart';

class Success_inform extends StatefulWidget {
  @override
  _Success_inform createState() => _Success_inform();
}

class _Success_inform extends State<Success_inform> {

  @override
  void initState() {
    super.initState();
    new Future.delayed(const Duration(seconds: 6), () => Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => (Touch())),
            (Route<dynamic> route) => false));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: new BoxDecoration(
          color: Color(0xffFFFFFF).withOpacity(0.7),
        ),
        child: Padding(
          padding: EdgeInsets.only(left: 120,right: 120,top: 70),
          child: Column(
            children: <Widget>[
              Image.asset('images/fire-cracker.png',width: 150,),
              SizedBox(height: 20),
              Text("MERCI !", style: TextStyle(fontSize: 70,fontFamily: "Poppins Bold",color: Colors.black)),
              SizedBox(height: 20),
              Text("Votre hôte a été informé", style: TextStyle(fontSize: 40,fontFamily: "Poppins-medium",color: Colors.black)),
              Text("et sera disponible sous peu.", style: TextStyle(fontSize: 40,fontFamily: "Poppins-medium",color: Colors.black)),
              SizedBox(height: 40),
              Text("S’il vous plait, prenez place.", style: TextStyle(fontSize: 40,fontFamily: "Poppins-medium",color: Colors.black)),
              SizedBox(height: 50),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  child: Image.asset("images/dhl.png",width: 150),
                  padding: EdgeInsets.only(right: 150),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

}

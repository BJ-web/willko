import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:willko/Accord.dart';
import 'package:willko/Success_check.dart';
import 'package:willko/Success_delivery.dart';

class Rdv_with extends StatefulWidget {
  @override
  _Rdv_with createState() => _Rdv_with();
}

class ArbitrarySuggestionType {
  String name, imgURL;

  ArbitrarySuggestionType(this.name, this.imgURL);
}

class _Rdv_with extends State<Rdv_with> {

  List<ArbitrarySuggestionType> suggestions = [
    new ArbitrarySuggestionType("Gilles Nembe",
        "images/homme-noir.jpg"),
    new ArbitrarySuggestionType("Jeff-Dany Ngouele",
        "images/homme-noir.jpg"),
    new ArbitrarySuggestionType("Safi Virginius",
        "images/homme-noir.jpg"),
  ];

  GlobalKey key =
  new GlobalKey<AutoCompleteTextFieldState<ArbitrarySuggestionType>>();

  AutoCompleteTextField<ArbitrarySuggestionType> textField;

  ArbitrarySuggestionType selected;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(40),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                      ),
                      Text("     Exodellices HQ",style: TextStyle(fontSize: 50,color: Color(0xffC2342E),fontFamily: "Poppins-medium")),
                      ButtonTheme(
                        padding: EdgeInsets.all(5.0),
                        minWidth: 140,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              side: BorderSide(color: Color(0xffC2342E))),
                          onPressed: () {
                            Navigator.push(
                                context, MaterialPageRoute(builder: (context) => Accord()));
                          },
                          color: Color(0xffC2342E),
                          textColor: Colors.white,
                          child: Text("suivant".toUpperCase(),
                              style: TextStyle(fontSize: 25, fontFamily: "Poppins")),
                        ),
                      )
                    ],
                  ),
                  Center(
                    child: Text("Qui est-ce que vous voulez voir ?",style: TextStyle(fontSize: 40,color: Color(0xffC2342E),fontFamily: "Sofia Pro")),
                  ),
                  SizedBox(height: 80),
                  Container(
                    width: 700,
                    height: 90,
                    decoration: new BoxDecoration(
                        color: Color(0xffF9C933),
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black12,
                            spreadRadius: 2,
                            blurRadius: 2,
                            offset: Offset(1, 3), // changes position of shadow
                          ),
                        ]
                    ),
                    child: selected != null? Container(
                      width: 700,
                      height: 80,
                      decoration: new BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Color(0xffFFFFFF).withOpacity(0.3)
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(7.0),
                        child: Align(
                          alignment: Alignment.center,
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundImage: AssetImage(
                                selected.imgURL,
                              ),
                              radius: 30,
                            ),
                            title: Text(
                              selected.name,style: TextStyle(fontSize: 23.0,
                                color: Color(0xffC2342E),
                                fontFamily: "Poppins-medium"),
                            ),
                          ),
                        ),
                      ),
                    ):
                    Container(
                      width: 700,
                      height: 90,
                        decoration: new BoxDecoration(
                          color: Color(0xffFFFFFF).withOpacity(0.4),
                          borderRadius: BorderRadius.circular(5),
                        ),
                      child: Padding(
                        padding: EdgeInsets.all(7.0),
                        child: Center(
                          child: AutoCompleteTextField<ArbitrarySuggestionType>(
                            decoration: new InputDecoration(
                                border: InputBorder.none,
                                hintText: "Votre recherche", prefixIcon: new Padding(padding: EdgeInsets.only(right: 20),
                                child: Icon(Icons.perm_identity,color: Color(0xffC2342E),size: 40))
                                ,hintStyle: TextStyle(fontSize: 23.0,
                                color: Color(0xffC2342E),
                                fontFamily: "Poppins-medium")),
                            itemSubmitted: (item) => setState(() => selected = item),
                            key: key,
                            style: TextStyle(fontSize: 23.0,
                                color: Color(0xffC2342E),
                                fontFamily: "Poppins-medium"),
                            suggestions: suggestions,
                            itemBuilder: (context, suggestion) => new Container(
                            width: 700,
                            height: 90,
                            decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Color(0xffFFFFFF)
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(7.0),
                              child: Align(
                                alignment: Alignment.center,
                                child: ListTile(
                                    leading: CircleAvatar(
                                      backgroundImage: AssetImage(
                                        suggestion.imgURL,
                                      ),
                                      radius: 30,
                                    ),
                                    title: new Text(
                                      suggestion.name,style: TextStyle(fontSize: 23.0,
                                        color: Color(0xffC2342E),
                                        fontFamily: "Poppins-medium"),
                                    )),
                              ),
                            ),
                          ),
                            itemSorter: (a, b) => a.name == b.name ? 0 : 1,
                            itemFilter: (suggestion, input) =>
                                suggestion.name.toLowerCase().contains(input.toLowerCase()),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 390),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Image.asset("images/GroupeLogo.png", width: 90),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

}

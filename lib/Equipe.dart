import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:willko/Staff_signin.dart';
import 'package:willko/Staff_signout.dart';

class Equipe extends StatefulWidget {
  @override
  _Equipe createState() => _Equipe();
}

class _Equipe extends State<Equipe> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Padding(
        padding: EdgeInsets.all(40),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    GestureDetector(
                      child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),
                    SizedBox(width: 240),
                    Text("Exodellices HQ",style: TextStyle(fontSize: 50,color: Color(0xffC2342E),fontFamily: "Poppins-medium")),
                  ],
                ),
                Center(
                  child: Text("Use your qrCode",style: TextStyle(fontSize: 40,color: Color(0xffC2342E),fontFamily: "Sofia Pro")),
                ),
                SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      child: Container(
                        width: 350,
                        height: 350,
                        decoration: new BoxDecoration(
                            color: Color(0xffFFFFFF).withOpacity(0.3),
                            borderRadius: new BorderRadius.only(
                                bottomRight: const Radius.circular(200.0),
                                bottomLeft: const Radius.circular(200.0),
                                topLeft: const Radius.circular(200.0),
                                topRight: const Radius.circular(200.0)
                            )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(7.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 40),
                              SvgPicture.asset(
                                  "images/checked-in.svg",
                                  color: Color(0xff5151EB),
                              ),
                              Text("Check-in",style: TextStyle(fontSize: 28.0,
                                  color: Colors.white,
                                  fontFamily: "Poppins"))
                            ],
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Staff_signin()));
                      },
                    ),
                    SizedBox(width: 50),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: 2,
                          height: 160,
                          color: Colors.white.withOpacity(0.5),
                        ),
                        Text("OR",style: TextStyle(fontSize: 23.0,
                            color: Colors.white,
                            fontFamily: "Poppins-medium")),
                        Container(
                          width: 2,
                          height: 160,
                          color: Colors.white.withOpacity(0.5),
                        )
                      ],
                    ),
                    SizedBox(width: 50),
                    GestureDetector(
                      child: Container(
                        width: 350,
                        height: 350,
                        decoration: new BoxDecoration(
                            color: Color(0xffFFFFFF).withOpacity(0.3),
                            borderRadius: new BorderRadius.only(
                                bottomRight: const Radius.circular(200.0),
                                bottomLeft: const Radius.circular(200.0),
                                topLeft: const Radius.circular(200.0),
                                topRight: const Radius.circular(200.0)
                            )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(7.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 40),
                              SvgPicture.asset(
                                "images/checked-out.svg",
                                color: Color(0xffC2342E),
                              ),
                              Text("Check-Out",style: TextStyle(fontSize: 28.0,
                                  color: Colors.white,
                                  fontFamily: "Poppins"))
                            ],
                          ),
                        ),
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Staff_signout()));
                      },
                    )
                  ],
                )
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                child: Image.asset("images/GroupeLogo.png", width: 90),
                padding: EdgeInsets.only(top: 20),
              ),
            )
          ],
        ),
      ),
    );
  }

}

import 'package:flutter/material.dart';
import 'package:willko/Success_check.dart';
import 'package:willko/Success_delivery.dart';

class Delivery_for extends StatefulWidget {
  @override
  _Delivery_for createState() => _Delivery_for();
}

class _Delivery_for extends State<Delivery_for> {


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Padding(
        padding: EdgeInsets.all(40),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    GestureDetector(
                      child: Icon(Icons.arrow_back,size: 43,color: Color(0xffC2342E)),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    ),
                    Text("     Exodellices HQ",style: TextStyle(fontSize: 50,color: Color(0xffC2342E),fontFamily: "Poppins-medium")),
                    ButtonTheme(
                      padding: EdgeInsets.all(5.0),
                      minWidth: 120,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            side: BorderSide(color: Color(0xffC2342E))),
                        onPressed: () {
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => Success_delivery()));
                        },
                        color: Color(0xffC2342E),
                        textColor: Colors.white,
                        child: Text("NEXT".toUpperCase(),
                            style: TextStyle(fontSize: 25, fontFamily: "Poppins")),
                      ),
                    )
                  ],
                ),
                Center(
                  child: Text("Who is the delivery for?",style: TextStyle(fontSize: 40,color: Color(0xffC2342E),fontFamily: "Sofia Pro")),
                ),
                SizedBox(height: 80),
                Container(
                  width: 700,
                  height: 80,
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Color(0xffFFFFFF).withOpacity(0.3)
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(7.0),
                    child: Align(
                      alignment: Alignment.center,
                      child: ListTile(
                        leading: CircleAvatar(
                          backgroundImage: AssetImage(
                            "images/homme-noir.jpg",
                          ),
                          radius: 30,
                        ),
                        title: Text(
                          "Cédrick Junior BÉKALÉ",style: TextStyle(fontSize: 23.0,
                            color: Color(0xffC2342E),
                            fontFamily: "Poppins-medium"),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset("images/GroupeLogo.png", width: 90),
            )
          ],
        ),
      ),
    );
  }

}

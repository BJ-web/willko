import 'package:flutter/material.dart';
import 'package:willko/Choix_1.dart';
import 'package:willko/Details_delivery.dart';
import 'package:willko/Equipe.dart';
import 'package:willko/Staff_depart.dart';

class Accueil extends StatefulWidget {
  @override
  _Accueil createState() => _Accueil();
}

class _Accueil extends State<Accueil> {

  String flag = "images/flag.png";
  String flag_title = "Fra";

  void _showPopupMenu() async {
    await showMenu(
      context: context,
      position: RelativeRect.fromLTRB(310, 100, 210,0),
      items: [
        PopupMenuItem<String>(
            child: GestureDetector(child: Row(children: <Widget>[Image.asset("images/flag.png", width: 33),Text(' Fra', style: TextStyle(fontSize: 16.0,
                color:Color(0xff1D1DE5),
                fontFamily: "Poppins"))]),onTap: (){
              setState(() {
                flag = "images/flag.png";
                flag_title = "Fra";
              });
              Navigator.pop(context);
            })),
        PopupMenuItem<String>(
            child: GestureDetector(child: Row(children: <Widget>[Image.asset("images/eng.jpg", width: 33),Text(' Fra', style: TextStyle(fontSize: 16.0,
    color: Color(0xff1D1DE5),
    fontFamily: "Poppins"))]),onTap: (){
              setState(() {
                flag = "images/eng.jpg";
                flag_title = "Eng";
              });
              Navigator.pop(context);
            }))
      ],
      elevation: 8.0,
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Padding(
        padding: EdgeInsets.all(50.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Image.asset("images/GroupeLogo.png", width: 90),
                Row(
                  children: <Widget>[
                    GestureDetector(
                      child: Container(
                        width: 90,
                        height: 43,
                        decoration: new BoxDecoration(
                            color: Color(0xff1D1DE5),
                            borderRadius: new BorderRadius.only(
                                bottomRight: const Radius.circular(8.0),
                                bottomLeft: const Radius.circular(8.0),
                                topLeft: const Radius.circular(8.0),
                                topRight: const Radius.circular(8.0)
                            )
                        ),
                        child: Padding(
                          padding: EdgeInsets.all(7.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Image.asset(flag, width: 33),
                              Text(flag_title,style: TextStyle(fontSize: 16.0,
                                  color: Colors.white,
                                  fontFamily: "Poppins"))
                            ],
                          ),
                        ),
                      ),
                      onTap: (){
                        _showPopupMenu();
                      },
                    ),
                    SizedBox(width: 20),
                    GestureDetector(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Image.asset("images/internet.png", width: 35),
                          SizedBox(width: 10),
                          Text("Équipes",style: TextStyle(fontSize: 30.0,
                              color: Color(0xffC2342E),
                              fontFamily: "Sofia Medium"))
                        ],
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => Equipe()));
                      },
                    )
                  ],
                )
              ],
            ),
            SizedBox(height: 50),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Bienvenue à",style: TextStyle(color: Color(0xffC2342E),fontFamily: "Poppins-medium",fontSize: 70)),
                    SizedBox(height: 20),
                    Text("DHL GABON",style: TextStyle(color: Color(0xffC2342E),fontFamily: "Poppins-light",fontSize: 70)),
                  ],
                ),
                GestureDetector(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset("images/Checkin-Logo.png",width: 90),
                      SizedBox(height: 5),
                      Text("Entrée",style: TextStyle(color: Color(0xffC2342E),fontFamily: "Sofia Medium",fontSize: 30)),
                    ],
                  ),
                  onTap: (){
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => Choix1()));
                  },
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                GestureDetector(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Image.asset("images/exit.png", width: 35),
                      SizedBox(width: 10),
                      Text("Départ",style: TextStyle(fontSize: 30.0,
                          color: Color(0xffC2342E),
                          fontFamily: "Sofia Medium"))
                    ],
                  ),
                  onTap: (){
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => Staff_depart()));
                  },
                ),
                Container(
                  width: 161,
                  height: 196,
                  decoration: new BoxDecoration(
                      color: Color(0xff1D1C1C).withOpacity(0.5),
                      borderRadius: new BorderRadius.only(
                          bottomRight: const Radius.circular(8.0),
                          bottomLeft: const Radius.circular(8.0),
                          topLeft: const Radius.circular(8.0),
                          topRight: const Radius.circular(8.0)
                      )
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(7.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Scan to visit",style: TextStyle(fontSize: 20.0,
                            color: Colors.white,
                            fontFamily: "Sofia Medium")),
                        Image.asset("images/qrcode.png", width: 140),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Image.asset("images/delivery.png", width: 35),
                      SizedBox(width: 10),
                      Text("Livraison",style: TextStyle(fontSize: 30.0,
                          color: Color(0xffC2342E),
                          fontFamily: "Sofia Medium"))
                    ],
                  ),
                  onTap: (){
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => Details_delivery()));
                  },
                )
              ],
            )
          ],
        ),
      ),
    );
  }

}

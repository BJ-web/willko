import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:signature/signature.dart';
import 'package:willko/Choix_1_1.dart';
import 'package:willko/Delivery_for.dart';
import 'package:willko/Success_check.dart';
import 'package:willko/Success_inform.dart';

class Accord extends StatefulWidget {
  @override
  _Accord createState() => _Accord();
}


class _Accord extends State<Accord> {
  ScrollController _scrollController = new ScrollController();
  bool up = true;
  bool sign = false;
  String dateDay;

  final SignatureController _controller = SignatureController(
    penStrokeWidth: 2,
    penColor: Color(0xff272727),
    exportBackgroundColor: Colors.white,
  );

  getCurrentDate(){
    var date = new DateTime.now().toString();
    var dateParse = DateTime.parse(date);
    var formattedDate = "${dateParse.day}-${dateParse.month}-${dateParse.year}";
    setState(() {
      dateDay = formattedDate;
    });
  }

  @override
  void initState() {
    super.initState();
    getCurrentDate();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Padding(
        padding: EdgeInsets.all(30),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                    color: Color(0xff62E09C),
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(5.0),topRight: Radius.circular(5.0)),
                ),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        child: Icon(Icons.arrow_back,size: 43,color: Color(0xffFFFFFF)),
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                      ),
                      SizedBox(width: 160),
                      Text("  Accord de non-divulgation",style: TextStyle(fontSize: 40,color: Color(0xffFFFFFF),fontFamily: "Poppins-medium")),
                    ],
                  ),
                ),
                Container(
                  width: 1200,
                  height: 600,
                  decoration: new BoxDecoration(
                    color: Color(0xffFFFFFF).withOpacity(0.7),
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5.0),bottomRight: Radius.circular(5.0)),
                  ),
                  child: Padding(
                    padding: EdgeInsets.only(left: 60,right: 60,bottom: 20),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 20),
                        Expanded(
                          flex: 1,
                          child: SingleChildScrollView(
                            controller: _scrollController,
                            scrollDirection: Axis.vertical,
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text("DHL GABON",style: TextStyle(fontSize: 35,color: Colors.black,fontFamily: "Poppins")),
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text("1-Proin ultricies erat ullamcorper risus congue, a suscipit enim faucibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce sodales molestie diam in vestibulum. Nunc dictum non est sed finibus. Praesent quis imperdiet ante. Nulla facilisi. Vestibulum lobortis eros dolor, vel aliquet libero pharetra eu.\n\n2-Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris quis arcu ut felis pellentesque imperdiet vel sed velit. Nunc molestie eleifend leo, et sagittis nisi finibus vel. Suspendisse orci quam, sollicitudin finibus scelerisque sit amet.\n\n3-Praesent et volutpat nisi, ac pellentesque est. Donec a augue est. Morbi eget ligula at ex consectetur luctus. Proin sit amet tincidunt nisi, nec pharetra neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisi massa, faucibus ut lacus eget, tempor molestie arcu.\n\n4-Praesent et volutpat nisi, ac pellentesque est. Donec a augue est. Morbi eget ligula at ex consectetur luctus. Proin sit amet tincidunt nisi, nec pharetra neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisi massa, faucibus ut lacus eget, tempor molestie arcu.\n\n5-Praesent et volutpat nisi, ac pellentesque est. Donec a augue est. Morbi eget ligula at ex consectetur luctus. Proin sit amet tincidunt nisi, nec pharetra neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisi massa, faucibus ut lacus eget, tempor molestie arcu.\n\n6-Praesent et volutpat nisi, ac pellentesque est. Donec a augue est. Morbi eget ligula at ex consectetur luctus. Proin sit amet tincidunt nisi, nec pharetra neque. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur nisi massa, faucibus ut lacus eget, tempor molestie arcu.",style: TextStyle(fontSize: 15,color: Colors.black,fontFamily: "Poppins Reg")),
                                ),
                                SizedBox(height: 40),
                                Container(
                                  width: 500,
                                  height: 200,
                                    decoration: new BoxDecoration(
                                      color: Color(0xffEDF7FD),
                                      borderRadius: BorderRadius.all(Radius.circular(26.0)),
                                    ),
                                  child: Stack(
                                    children: <Widget>[
                                      sign?Container():
                                      Center(
                                        child: Text("Signez ici",style: TextStyle(fontSize: 80,fontFamily: "Poppins-medium",color: Color(0xffF0F0F0))),
                                      ),
                                      GestureDetector(
                                        child:
                                        Signature(
                                            controller: _controller,
                                            height: 190,
                                            width: 490,
                                            backgroundColor: Color(0xffEDF7FD).withOpacity(0.2)
                                        ),
                                        onPanStart: (tapUpDetails){
                                          setState(() {
                                            sign = true;
                                          });
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                GestureDetector(
                                  child: Text("Effacer", style: TextStyle(fontSize: 18,fontFamily: "Poppins")),
                                  onTap: (){
                                    setState(() => _controller.clear());
                                    sign = false;
                                  },
                                ),
                                SizedBox(height: 20),
                                Row(
                                  children: <Widget>[
                                    Flexible(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text("Jeremy Stoss",style: TextStyle(color: Color(0xff272727),fontSize: 18,fontFamily: "Poppins")),
                                          Container(
                                            width: 450,
                                            height: 1,
                                            color: Color(0xffBEBEBE),
                                          ),
                                          Text("Nom",style: TextStyle(color: Color(0xffBEBEBE),fontSize: 20,fontFamily: "Poppins-medium")),
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 60),
                                    Flexible(
                                      flex: 1,
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(dateDay,style: TextStyle(color: Color(0xff272727),fontSize: 18,fontFamily: "Poppins")),
                                          Container(
                                            width: 450,
                                            height: 1,
                                            color: Color(0xffBEBEBE),
                                          ),
                                          Text("Date",style: TextStyle(color: Color(0xffBEBEBE),fontSize: 20,fontFamily: "Poppins-medium")),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(height: 20),
                                Row(
                                  children: <Widget>[
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text("Une copie de l’accord signé sera envoyée à :",style: TextStyle(fontSize: 16,color: Color(0xff707070),fontFamily: "Poppins-medium")),
                                    ),
                                    Align(
                                      alignment: Alignment.topLeft,
                                      child: Text(" j.stoss@afrimarket.com",style: TextStyle(fontSize: 16,color: Color(0xff272727),fontFamily: "Poppins-medium")),
                                    ),
                                    SizedBox(width: 100),
                                    ButtonTheme(
                                      padding: EdgeInsets.all(5.0),
                                      minWidth: 170,
                                      height: 55,
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30.0),
                                            side: BorderSide(color:Color(0xffBEBEBE))),
                                        onPressed: () {
                                          Navigator.push(
                                          context, MaterialPageRoute(builder: (context) => Success_inform()));
                                        },
                                        color:sign?Color(0xff1D1DE5):Color(0xffBEBEBE),
                                        textColor: Colors.white,
                                        child: Text("D'ACCORD".toUpperCase(),
                                            style: TextStyle(fontSize: 25, fontFamily: "Poppins")),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            GestureDetector(
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: Container(
                      width: 50,
                      height: 50,
                      color: Colors.white,
                      child: Icon(up?Icons.arrow_downward:Icons.arrow_upward,color: Color(0xff27A0EC)),
                    ),
                  )
              ),
              onTap: (){
                up?
                setState(() {
                  up = false;
                  _scrollController.animateTo(
                    _scrollController.position.maxScrollExtent,
                    duration: Duration(seconds: 1),
                    curve: Curves.fastOutSlowIn,
                  );
                }):
                setState(() {
                  up = true;
                  _scrollController.animateTo(
                    _scrollController.position.minScrollExtent,
                    duration: Duration(seconds: 1),
                    curve: Curves.fastOutSlowIn,
                  );
                });
              },
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(top: 40),
                child: Image.asset("images/GroupeLogo.png", width: 90),
              ),
            )
          ],
        ),
      ),
    );
  }

}

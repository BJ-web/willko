import 'package:flutter/material.dart';
import 'package:willko/TouchScreen.dart';

class Success extends StatefulWidget {
  final bool statut;
  Success({Key key, @required this.statut}) : super(key: key);
  @override
  _Success createState() => _Success(statut: statut);
}

class _Success extends State<Success> {
  final bool statut;
  _Success({Key key, @required this.statut});

  @override
  void initState() {
    super.initState();
    new Future.delayed(const Duration(seconds: 6), () => Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => (Touch())),
            (Route<dynamic> route) => false));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: new BoxDecoration(
          color: Color(0xffFFFFFF).withOpacity(0.7),
        ),
        child: Padding(
          padding: EdgeInsets.all(7.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Stack(
                  children: <Widget>[
                    CircleAvatar(
                      backgroundImage: AssetImage(
                        "images/homme-noir.jpg",
                      ),
                      radius: 100,
                    ),

                    Positioned(
                      bottom: 0.0,
                      right: 30.0,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(19),
                        ),
                        height: 30,
                        width: 30,
                        child: Center(
                          child: Container(
                            decoration: BoxDecoration(
                              color: statut
                                  ?Color(0xff62E09C)
                                  :Color(0xffE62121),
                              borderRadius: BorderRadius.circular(19),
                            ),
                            height: 30,
                            width: 30,
                          )
                          ,
                        ),
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 10),
                Text("Hi Claude", style: TextStyle(fontSize: 70,fontFamily: "Poppins Bold",color: Colors.black)),
                SizedBox(height: 20),
                Text(statut?"You’re now checked-in":"You’re now checked-out", style: TextStyle(fontSize: 40,fontFamily: "Poppins-medium",color: Colors.black))
              ],
            ),
          ),
        ),
      ),
    );
  }

}

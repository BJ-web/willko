import 'package:flutter/material.dart';
import 'package:willko/Accueil.dart';

class Touch extends StatefulWidget {
  @override
  _Ts createState() => _Ts();
}

class _Ts extends State<Touch> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Color(0xffF9C933),
        body: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 80),
                  Image.asset("images/dhl.png",width: 600,),
                  SizedBox(height: 100),
                  ButtonTheme(
                      minWidth: 280,
                      height: 55,
                      child: RaisedButton(
                        splashColor: Colors.grey,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.red,width: 3.0)),
                        color: Color(0xffF9C933),
                        textColor: Color(0xffC2342E),
                        padding: EdgeInsets.all(8.0),
                        onPressed: () {
                          Navigator.push(
                              context, MaterialPageRoute(builder: (context) => Accueil()));
                        },
                        child: Text(
                          "TOUCHEZ L’ÉCRAN".toUpperCase(),
                          style: TextStyle(
                              fontSize: 20.0,
                              color: Color(0xffC2342E),
                              fontFamily: "Poppins"
                          ),
                        ),
                      )
                  )
                ],
              ),
            ),
            Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 10),
                    child: Image.asset("images/GroupeLogo.png", width: 90),
                  )
                ),
              ],
            )
          ],
        ),
      ),
      onTap: (){
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => Accueil()));
      },
    );
  }

}

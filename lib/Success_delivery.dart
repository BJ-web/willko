import 'package:flutter/material.dart';
import 'package:willko/TouchScreen.dart';

class Success_delivery extends StatefulWidget {
  @override
  _Success_delivery createState() => _Success_delivery();
}

class _Success_delivery extends State<Success_delivery> {

  @override
  void initState() {
    super.initState();
    new Future.delayed(const Duration(seconds: 6), () => Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => (Touch())),
            (Route<dynamic> route) => false));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Color(0xffF9C933),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: new BoxDecoration(
          color: Color(0xffFFFFFF).withOpacity(0.7),
        ),
        child: Padding(
          padding: EdgeInsets.all(140.0),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset('images/fire-cracker.png',width: 150,),
                SizedBox(height: 20),
                Text("MERCI !", style: TextStyle(fontSize: 70,fontFamily: "Poppins Bold",color: Colors.black)),
                SizedBox(height: 20),
                Text("Le destinataire a été informé", maxLines: 2, style: TextStyle(fontSize: 40,fontFamily: "Poppins-medium",color: Colors.black)),
                Text("de la livraison.", maxLines: 2, style: TextStyle(fontSize: 40,fontFamily: "Poppins-medium",color: Colors.black)),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    child: Image.asset("images/dhl.png",width: 100),
                    padding: EdgeInsets.only(right: 110),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

}

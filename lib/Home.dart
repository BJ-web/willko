import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:willko/TouchScreen.dart';

class MyApp extends StatefulWidget {
  @override
  _MyHome createState() => _MyHome();
}

class _MyHome extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
   return Scaffold(
     body: SingleChildScrollView(
       child: Center(
         child: Stack(
           children: <Widget>[
             Container(
               width: MediaQuery.of(context).size.width,
               height: MediaQuery.of(context).size.height,
               decoration: BoxDecoration(
                 image: DecorationImage(
                   image: AssetImage("images/Background.png"),
                   fit: BoxFit.cover,
                 ),
               ),
               child: null,
             ),
             Opacity(
               opacity: 0.9,
               child: Container(
                 width: MediaQuery.of(context).size.width,
                 height: MediaQuery.of(context).size.height,
                 decoration: BoxDecoration(
                   image: DecorationImage(
                     image: AssetImage("images/Building.png"),
                     fit: BoxFit.cover,
                   ),
                 ),
                 child: null,
               ),
             ),
             Padding(
                 padding: EdgeInsets.only(left: 35,right: 35,top: 120,bottom: 120),
                 child: Container(child: Stack(
                   children: <Widget>[
                     Align(
                       alignment: Alignment.centerRight,
                       child: Container(
                         width: MediaQuery.of(context).size.width/1.50,
                         height: MediaQuery.of(context).size.height-240,
                         decoration: BoxDecoration(
                           image: DecorationImage(
                             image: AssetImage("images/Building2.png"),
                             fit: BoxFit.cover,
                           ),
                         ),
                         child: null,
                       ),
                     ),
                     Container(
                       width: MediaQuery.of(context).size.width/2.50,
                       height: MediaQuery.of(context).size.height-240,
                       decoration: new BoxDecoration(
                           color: Colors.white,
                           borderRadius: new BorderRadius.only(
                             bottomRight: const Radius.circular(50.0),
                           )
                       ),
                       child: Padding(
                         padding: EdgeInsets.only(left: 60,top: 27,right: 60),
                         child: Align(
                           alignment: Alignment.topLeft,
                           child: Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: <Widget>[
                               Image.asset(
                                 "images/Logo.png",width: 150,
                               ),
                               SizedBox(height: 50),
                               Text("Welcome",style: TextStyle(fontSize: 21, fontFamily: "Sofia Pro",color: Color(0xff4A4647))),
                               SizedBox(height: 10),
                               Text("Let’s Help you reduce",style: TextStyle(fontSize: 22, fontFamily: "Poppins-medium",color: Color(0xff4A4647),fontStyle: FontStyle.normal)),
                               RichText(
                                 text: TextSpan(
                                     children: [
                                       TextSpan(
                                           text: "Visitor ",
                                           style: TextStyle(fontSize: 22,fontFamily: "Poppins",color: Color(0xff5151EB))
                                       ),
                                       TextSpan(
                                           text: "check-in ",
                                           style: TextStyle(fontSize: 22, fontFamily: "Poppins-medium",color: Color(0xff4A4647))
                                       ),
                                       TextSpan(
                                           text: "frustration",
                                           style: TextStyle(fontSize: 22, fontFamily: "Poppins",color: Color(0xffEB5454))
                                       )
                                     ]
                                 ),
                               ),
                               SizedBox(height: 30),
                               Card(
                                 elevation: 2,
                                 child: Container(
                                   height: 35,
                                   child: Padding(
                                       padding: EdgeInsets.only(left: 10,right: 10,bottom: 0),
                                       child : PinCodeTextField(
                                         length: 8,
                                         textInputType: TextInputType.number,
                                         obsecureText: false,
                                         animationType: AnimationType.scale,
                                         textStyle: TextStyle(fontSize: 18, fontFamily: "Montserrat",color: Color(0xffEB5454)),
                                         pinTheme: PinTheme(
                                           activeColor: Color(0xffEB5454),
                                           inactiveColor: Color(0xffEB5454),
                                           inactiveFillColor: Color(0xffEB5454),
                                           selectedColor: Color(0xff5151EB),
                                           shape: PinCodeFieldShape.underline,
                                           borderRadius: BorderRadius.circular(5),
                                           fieldHeight: 30,
                                           fieldWidth: 25,
                                           borderWidth: 1.0,
                                           activeFillColor: Color(0xffEB5454),
                                         ),
                                         animationDuration: Duration(milliseconds: 300),
                                         enableActiveFill: false,
                                         onCompleted: (v) {
                                           print("Completed");
                                         },
                                         onChanged: (value) {
                                           print(value);
                                           setState(() {
                                             //currentText = value;
                                           });
                                         },
                                       )
                                   ),
                                 ),
                               ),
                               SizedBox(height: 30),
                               Align(
                                 alignment: Alignment.center,
                                 child: ButtonTheme(
                                   padding: EdgeInsets.all(5.0),
                                   minWidth: (MediaQuery.of(context).size.width/2.50)-150,
                                   child: RaisedButton(
                                     shape: RoundedRectangleBorder(
                                         borderRadius: BorderRadius.circular(30.0),
                                         side: BorderSide(color: Color(0xff1D1DE5))),
                                     onPressed: () {
                                       Navigator.push(
                                           context, MaterialPageRoute(builder: (context) => Touch()));
                                     },
                                     color: Color(0xff1D1DE5),
                                     textColor: Colors.white,
                                     child: Text("CONNEXION".toUpperCase(),
                                         style: TextStyle(fontSize: 16, fontFamily: "Poppins")),
                                   ),
                                 ),
                               ),
                             ],
                           ),
                         ),
                       ),
                     )
                   ],
                 ),)
             ),
           ],
         ),
       ),
     ),
   );
  }
}